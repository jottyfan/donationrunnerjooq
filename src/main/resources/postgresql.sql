create table t_agerange(id int generated always as identity primary key, name text not null unique);
create table t_kilometer(id int generated always as identity primary key, name text not null unique, kilometer numeric(3,1) not null unique);
create table t_donationgoal(id int generated always as identity primary key, name text not null unique);
create table t_runner(id int generated always as identity primary key, alias text not null unique, forename text not null, surname text not null, street text not null, 
                      zip text not null, city text not null, mail text not null, phone text, 
                      fk_kilometer int not null references t_kilometer(id), 
                      fk_agerange int not null references t_agerange(id),
                      fk_donationgoal int references t_donationgoal(id));
create table t_donator(id int generated always as identity primary key, forename text not null, surname text not null, company text, street text not null, zip text not null, 
                       city text not null, mail text not null, phone text);
create table t_donation(fk_donator int not null references t_donator(id), fk_runner int not null references t_runner(id), 
                        kmdonation float not null check (kmdonation > 0));
create table t_property(id int generated always as identity primary key, key text not null unique, value text not null);                        

create or replace view v_donation as
select r.alias, k.kilometer, k.kilometer * sum(coalesce(d.kmdonation, 0)) as donation_total
from t_runner r
left join t_kilometer k on k.id = r.fk_kilometer
left join t_donation d on d.fk_runner = r.id
group by r.alias, k.kilometer;

create or replace view v_donator as
select d.id, d.forename, d.surname, d.company, d.street, d.zip, d.city, d.mail, d.phone,
    count(n.fk_runner) as runners,
    sum(n.kmdonation * k.kilometer) as euro
from t_donator d
left join t_donation n on n.fk_donator = d.id
left join t_runner r on r.id = n.fk_runner
left join t_kilometer k on k.id = r.fk_kilometer
group by d.id, d.forename, d.surname, d.company, d.street, d.zip, d.city, d.mail, d.phone;

create or replace view v_donatorsum as
select d.fk_donator, sum(d.kmdonation * k.kilometer) as euro, sum(kilometer) as kilometer, count(d.fk_runner) as runners  
from t_donation d
left join t_runner r on r.id = d.fk_runner 
left join t_kilometer k on k.id  = r.fk_kilometer 
group by d.fk_donator;

insert into t_agerange(name) values ('Kind bis 12 Jahre'), ('Jugendlicher bis 18 Jahre'), ('Erwachsener');
insert into t_kilometer(name, kilometer) values ('Minimarathon (2,7 km)', 2.7), ('10 km', 10), ('Halbmarathon (21,1 km)', 21.1), ('Marathon (42,2 km)', 42.2); 
insert into t_property(key, value) values ('password', 'test');
insert into t_donationgoal(name) values ('Jugendverband (Sportarbeit/Musik- & Kulturarbeit)'), ('Jugendhäuser (Kinder-& Jugendhaus Chilli/Jugendtreff Upstairs)');
